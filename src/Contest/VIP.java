package Contest;

class VIP extends TiketKonser {
    //membuat attriibute nama dan harga tiket
    private static final String namaTiket = "VIP";
    private static final double hargaTiket = 2000000.0;
    
    //untuk mengambil nama tiket
    public static String getNamaTiket() {
        return namaTiket;
    }
    
    //untuk mengambil harga tiket
    public static double getHargaTiket() {
        return hargaTiket;
    }

    @Override
    public double harga(double hargaTiket) {
        return hargaTiket;
    }
}