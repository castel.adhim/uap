package Contest;

abstract class TiketKonser implements HargaTiket {
    public void cetakHasilPesanan(String namaPemesan, String namaTiket, double hargaTiket) {
        //membuat object pemesanan tiket untuk mencetak detail pemesanan dengan parameter seperti dibawah
        PemesananTiket pemesananTiket = new PemesananTiket(namaPemesan, namaTiket, hargaTiket);
        System.out.println("----- Detail Pesanan -----");
        System.out.println("Nama Pemesan: " + pemesananTiket.getNamaPemesan());
        System.out.println("Kode Booking: " + pemesananTiket.getKodePesanan());
        System.out.println("Tanggal Pesanan: " + pemesananTiket.getTanggalPesanan());
        System.out.println("Tiket yang dipesan: " + pemesananTiket.getNamaTiket());
        System.out.printf("Total Harga: Rp%,.2f %n", pemesananTiket.getTotalHarga());
    }
}