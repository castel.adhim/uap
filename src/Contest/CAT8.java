package Contest;

class CAT8 extends TiketKonser {
    //membuat attriibute nama dan harga tiket
    private static final String namaTiket = "CAT8";
    private static final double hargaTiket = 500000.0;
    
    //untuk mengambil nama tiket
    public static String getNamaTiket() {
        return namaTiket;
    }
    
    //untuk mengambil harga tiket
    public static double getHargaTiket() {
        return hargaTiket;
    }
    

    @Override
    public double harga(double hargaTiket) {
        return hargaTiket;
    }
}