package Contest;

class PemesananTiket {
    private String namaPemesan;
    private String kodePesanan;
    private String tanggalPesanan;
    private String namaTiket;
    private double totalHarga;
    
    //method yang digunakan untuk melakukan pemesanan tiket dengan parameter nama, nama tiket, dan harga
    public PemesananTiket(String namaPemesan, String namaTiket, double totalHarga) {
        this.namaPemesan = namaPemesan;
        this.kodePesanan = Main.generateKodeBooking();
        this.tanggalPesanan = Main.getCurrentDate();
        this.namaTiket = namaTiket;
        this.totalHarga = totalHarga;
        System.out.println("");
    }
    
    //untuk mengambil namaPemesan yang akan digunakan di tiket konser
    public String getNamaPemesan() {
        return namaPemesan;
    }
    //untuk mengambil kodePemesanan
    public String getKodePesanan() {
        return kodePesanan;
    }
    //untuk mengambil tanggalPemesanan
    public String getTanggalPesanan() {
        return tanggalPesanan;
    }
    //untuk mengambil nama jenis tiket
    public String getNamaTiket() {
        return namaTiket;
    }
    //untuk mengambil total harga
    public double getTotalHarga() {
        return totalHarga;
    }
}