/*
 * UAP PEMLAN 2023
 * DURASI: 120 MENIT
 * TEMPAT PENGERJAAN: DARING/LURING
 * =================================================================
 * Semangat mengerjakan UAP teman-teman
 * Jangan lupa berdoa untuk hasil yang terbaik
 */

package Contest;

import java.util.*;
import java.text.SimpleDateFormat;


public class Main {
    public static void main(String[] args) {
        //inisiasi scan
        Scanner scan = new Scanner (System.in);
        
        System.out.println("Selamat datang di pemesanan tiket coldplay!");
        
        boolean validInput = false;
        do{//penggunaan do while untuk melakukan perulangan pada pemesanan
        try {
            System.out.print("Masukkan nama pemesan: ");
            String nama = scan.nextLine();//digunakan untuk menginput nama pemesan
            
            
        //tampilan untuk list jenis tiket yang dapat dipeaan    
        System.out.println("Pilih jenis tiket:");
        System.out.println("1. CAT8 (Rp500,000.00)");
        System.out.println("2. CAT1 (Rp1,000,000.00)");
        System.out.println("3. FESTIVAL (Rp1,500,000.00");
        System.out.println("4. VIP (Rp2,000,000.00)");
        System.out.println("5. VVIP (UNLIMITED EXPERIENCE) (Rp2,500,000.00)");
        System.out.print("Masukkan pilihan: ");
        int jenisTiket = scan.nextInt();
        
        
          String namaTiket = "";
         double hargaTiket = 0.0;
        switch (jenisTiket) {//menggunakan switch case untuk memilih tiket yang ingin dipilih
                case 1:
                    //mengambil nama dan harga tiket CAT8
                    namaTiket = CAT8.getNamaTiket();
                    hargaTiket = CAT8.getHargaTiket();
                    break;

                case 2:
                    //mengambil nama dan harga tiket CAT1
                    namaTiket = CAT1.getNamaTiket();
                    hargaTiket = CAT1.getHargaTiket();
                    break;
                case 3:
                    //mengambil nama dan harga tiket Festival
                    namaTiket = FESTIVAL.getNamaTiket();
                    hargaTiket = FESTIVAL.getHargaTiket();
                    break;
                case 4:
                    //mengambil nama dan harga tiket VIP
                    namaTiket = VIP.getNamaTiket();
                    hargaTiket = VIP.getHargaTiket();
                    break;
                case 5:
                    //mengambil nama dan harga tiket CAT8
                    namaTiket = VVIP.getNamaTiket();
                    hargaTiket = VVIP.getHargaTiket();
                    break;
                default:
                    throw new IllegalArgumentException("Error: Input tidak valid. Masukkan angka (1-5) untuk jenis tiket.");
            } 
            
                TiketKonser tiketKonser = new TiketKonser() {//untuk memanggil class tiket konser dengan membuat object baru
                
                @Override
                public double harga(double hargaTiket) {
                return hargaTiket;
                }
            };
                tiketKonser.cetakHasilPesanan(nama, namaTiket, hargaTiket);
                break;
                 
        } catch (InputMismatchException e) {//exception yang digunakan untuk memastikan input bukan huruf dan angkat bulat
            System.out.println("Error: Input tidak valid! Pilihan tiket harus berupa angka bulat.");
            System.out.println("");
        } catch (IllegalArgumentException e) {//exception yang digunakan untuk memastikan pilihan tiket tidak melebihi 5 
            System.out.println("Error: Input tidak valid. Masukkan angka (1-5) untuk jenis tiket.");
            System.out.println("");
        }
    }while (!validInput);
        scan.close();
    }

    /*
     * Jangan ubah isi method dibawah ini, nama method boleh diubah
     * Method ini dipanggil untuk mendapatkan kode pesanan atau kode booking
     * Panggil method ini untuk kelengkapan mencetak output nota pesanan
     */
    public static String generateKodeBooking() {
        StringBuilder sb = new StringBuilder();
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        int length = 8;

        for (int i = 0; i < length; i++) {
            int index = (int) (Math.random() * characters.length());
            sb.append(characters.charAt(index));
        }

        return sb.toString();
    }

    /*
     * Jangan ubah isi method dibawah ini, nama method boleh diubah
     * Method ini dipanggil untuk mendapatkan waktu terkini
     * Panggil method ini untuk kelengkapan mencetak output nota pesanan
     */
    public static String getCurrentDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date currentDate = new Date();
        return dateFormat.format(currentDate);
    }
}