package Contest;

class VVIP extends TiketKonser {
    //membuat attriibute nama dan harga tiket
    private static final String namaTiket = "VVIP (UNLIMITED EXPERIENCE)";
    private static final double hargaTiket = 2500000.0;
    
    //untuk mengambil nama tiket
    public static String getNamaTiket() {
        return namaTiket;
    }
    
    //untuk mengambil nama tiket
    public static double getHargaTiket() {
        return hargaTiket;
    }

    @Override
    public double harga(double hargaTiket) {
        return hargaTiket;
    }
}